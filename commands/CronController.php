<?php
/**
 * Created by PhpStorm.
 * User: atkach
 * Date: 05.07.17
 * Time: 16:14
 */

namespace app\commands;

use Yii;
use app\models\DashboardUsers;
use fedemotta\cronjob\models\CronJob;
use yii\console\Controller;

/**
 * Test controller
 */
class CronController extends Controller {

    public function actionIndex() {
        echo "cron service runnning  \n";
    }

    public function actionUpdateTokens() {
        $users = DashboardUsers::find()->all();
        foreach ($users as $user) {
            $user->token = bin2hex(openssl_random_pseudo_bytes(32));
            if (!$user->update()) {
                echo 'error updating tokens for users';
                return false;
            }
            else {
                //echo $user->token;
                $emailto = $user->email;
                $token = $user->token;
                self::actionMail($emailto, $token);
            }
        }
        echo 'users\' tokens updated';
        return true;
    }

    public static function actionMail($to, $token) {
        echo "Sending mail to $to \n";

        Yii::$app->mailer->compose()
            ->setFrom('admin@ohmnication.com')
            ->setTo($to)
            ->setSubject('Your New Token For ohmnication.com')
            ->setTextBody($token)
            ->send();
    }

}