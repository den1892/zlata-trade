<?php

use yii\db\Migration;
use yii\db\Schema;

class m170804_084850_add_options_table extends Migration
{

    public function up()
    {
        $this->createTable('options', [
            'id' => Schema::TYPE_PK,
            'option_name' => Schema::TYPE_STRING . ' NOT NULL',
            'option_group' => Schema::TYPE_STRING . ' NOT NULL',
            'option_value' => Schema::TYPE_TEXT . ' NULL',
        ]);
    }

    public function down()
    {
        $this->dropTable('options');
    }

}
