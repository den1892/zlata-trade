<?php

use yii\db\Migration;
use yii\db\Schema;

class m170726_091600_zlata_tables extends Migration
{

    public function up()
    {
        $this->createTable('posts', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'excerpt' => $this->string(100),
            'type' => Schema::TYPE_STRING . ' NOT NULL',
            'content' => Schema::TYPE_TEXT,
            'author_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->defaultValue(1),
            'updated_at' => 'timestamp on update current_timestamp',
            'created_at' => $this->dateTime() . ' DEFAULT NOW()',
        ]);

        $this->createTable('post_categories', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . ' NOT NULL',
            'create_at' => $this->timestamp()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('posts');
        $this->dropTable('post_categories');
        return true;
    }

}
