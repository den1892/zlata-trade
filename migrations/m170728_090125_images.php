<?php

use yii\db\Migration;
use yii\db\Schema;

class m170728_090125_images extends Migration
{

    public function up()
    {
        $this->createTable('images', [
            'id' => Schema::TYPE_PK,
            'image_filename' => Schema::TYPE_STRING . ' NOT NULL',
            'image_filepath' => Schema::TYPE_TEXT . ' DEFAULT NULL',
            'image_url' => Schema::TYPE_STRING . ' NOT NULL',
            'image_filesize' =>Schema::TYPE_INTEGER,
            'created_at' => $this->dateTime() . ' DEFAULT NOW()',
        ]);
    }

    public function down()
    {
        $this->dropTable('images');
    }

}
