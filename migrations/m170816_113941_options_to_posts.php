<?php

use yii\db\Migration;
use yii\db\Schema;

class m170816_113941_options_to_posts extends Migration
{
    public function up()
    {
        $this->createTable('options_to_posts', [
            'id' => Schema::TYPE_PK,
            'option_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'post_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
    }

    public function down()
    {
        $this->dropTable('options');
    }
}
