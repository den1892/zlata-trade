<?php

use yii\db\Migration;
use yii\db\Schema;

class m170728_090909_images_to_post extends Migration
{
    public function up()
    {
        $this->createTable('images_to_post', [
            'id' => Schema::TYPE_PK,
            'image_id' =>Schema::TYPE_INTEGER,
            'post_id' =>Schema::TYPE_INTEGER,
        ]);
    }

    public function down()
    {
        $this->dropTable('images_to_post');
    }

}
