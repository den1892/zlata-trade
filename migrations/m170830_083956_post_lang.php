<?php

use yii\db\Migration;

class m170830_083956_post_lang extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->execute("
        CREATE TABLE IF NOT EXISTS `postLang` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `post_id` int(11) NOT NULL,
        `language` varchar(6) NOT NULL,
        `title` varchar(255) NOT NULL,
        `excerpt` varchar(255) NOT NULL,
        `content` TEXT NOT NULL,
        PRIMARY KEY (`id`),
        KEY `post_id` (`post_id`),
        KEY `language` (`language`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
        ALTER TABLE `postLang`
        ADD CONSTRAINT `postlang_ibfk_1` 
        FOREIGN KEY (`post_id`) REFERENCES `post` (`id`) 
        ON DELETE CASCADE ON UPDATE CASCADE;
        ");
    }

    public function down()
    {
        $this->dropTable('postLang');
    }
}
