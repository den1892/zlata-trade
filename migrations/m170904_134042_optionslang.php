<?php

use yii\db\Migration;

class m170904_134042_optionslang extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->execute("
        CREATE TABLE IF NOT EXISTS `options_lang` (
        `id` int(11) NOT NULL AUTO_INCREMENT,
        `option_id` int(11) NOT NULL,
        `language` varchar(6) NOT NULL,
        `content` varchar(255) NULL,
        PRIMARY KEY (`id`),
        KEY `option_id` (`option_id`),
        KEY `language` (`language`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8;
        ALTER TABLE `options_lang`
        ADD CONSTRAINT `options_lang_ibfk_1` 
        FOREIGN KEY (`option_id`) REFERENCES `options` (`id`) 
        ON DELETE CASCADE ON UPDATE CASCADE;
        ");
    }

    public function down()
    {
        $this->dropTable('options_lang');
    }
}
