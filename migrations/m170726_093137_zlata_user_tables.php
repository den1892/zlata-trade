<?php

use yii\db\Schema;
use yii\db\Migration;

class m170726_093137_zlata_user_tables extends Migration
{

    public function up()
    {
        $this->createTable('dashboard_users', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
            'username' => Schema::TYPE_STRING . ' NOT NULL',
            'password' => Schema::TYPE_STRING . ' NOT NULL',
            'email' => Schema::TYPE_STRING . ' NOT NULL',
            'token' => Schema::TYPE_STRING . ' NOT NULL',
        ]);

        $this->createTable('dashboard_roles', [
            'id' => Schema::TYPE_PK,
            'role_name' => Schema::TYPE_STRING . ' NOT NULL',
            'permissions' => Schema::TYPE_STRING . ' NOT NULL',
        ]);

        $this->createTable('dashboard_user_to_role', [
            'id' => Schema::TYPE_PK,
            'user_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'role_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        return true;
    }

    public function down()
    {
        $this->dropTable('dashboard_users');
        $this->dropTable('dashboard_roles');
        $this->dropTable('dashboard_user_to_role');
        return true;
    }

}
