<?php

/* @var $this yii\web\View */

$mainInfo = [
    'acceptance' => [
        'title' => 'Закупка и цены зерновых и масленичных культур на Юге Украины',
        'meta_description' => 'Закупка различных зерновых и масленичных культур: Ячмень фуражный, Пшеница фуражная, Пшеница 2-3 класса, Рапс, Кукуруза',
        'meta_keywords' => 'закупка рапса, зерно цена, закупка кукурузы, зерно купить, ячмень фуражный',
        'h1' => 'Закупка зерновых<br>и масличных культур',
        'img' => '/assets/images/services_img1.png',
    ],
    'realization' => [
        'title' => 'Цены на реализацию минеральных удобрений в компании Zlata Trade',
        'meta_description' => 'Цены на реализацию минеральных удобрений в Zlata Trade: Купить аммиачную селитру, семена подсолнечника и др по доступным ценам',
        'meta_keywords' => 'сульфат аммония, селитра аммиачная, амофос, карбамид',
        'h1' => 'Реализация <br> минеральных удобрений',
        'img' => '/assets/images/services_img2.jpg',
    ],
    'transport' => [
        'title' => 'Услуги Железнодорожного и Автомобильного экспедирования в Украине',
        'meta_description' => 'Zlata Trade предоставляет услуги железнодорожного и автомобильного экспедирования грузов по Украине',
        'meta_keywords' => 'Услуги экспедирования, автоэкспедиция, ж/д экспедирование',
        'h1' => 'Автоуслуги<br>и ж/д экспедирование',
        'img' => '/assets/images/services_img3.png',
    ],
];
$mainInfo = $mainInfo[$serviceKey];
$h1Text = 'Деятельность';

$this->title = $mainInfo['title'];
$this->registerMetaTag([
    'name' => 'description',
    'content' => $mainInfo['meta_description'],
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $mainInfo['meta_keywords'],
]);

echo $this->render('/templates/serviceInfo', ['mainInfo' => $mainInfo, 'h1Text' => $h1Text, 'services' => $services, 'serviceKey' => $serviceKey]);
