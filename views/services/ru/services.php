<?php

use yii\helpers\Url;

$this->title = 'Услуги закупки, перевозки и реализации зерновых культур Zlata Trade';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Закупка зерновых и маслиничных культур, автоуслуги и экспедирование, а также реализация минеральных удобрений в Украине',
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'Зерно закупка, закупка зерновых культур, закупка зерна цена, экспедирование, транспортное экспедирование',
]);

?>


<?php 

$services = [
    [
        'title' => 'Закупки зерновых<br>и масличных культур',
        'img' => '/rest/dist-v1.1/img/service1.png',
        'url' => Url::to(['services/service-info', 'service' => 'acceptance', 'language' => Yii::$app->language]),
    ],
    [
        'title' => 'Автоуслуги<br>и ж/д экспедирование',
        'img' => '/rest/dist-v1.1/img/service3.png',
        'url' => Url::to(['services/service-info', 'service' => 'transport', 'language' => Yii::$app->language]),
    ],
    [
        'title' => 'Реализация<br>минеральных удобрений',
        'img' => '/rest/dist-v1.1/img/service2.png',
        'url' => Url::to(['services/service-info', 'service' => 'realization', 'language' => Yii::$app->language]),
    ],
];

$viewText = 'Подробнее';
$h1Text = 'Деятельность';
echo $this->render('/templates/services.php', ['services' => $services, 'viewText' => $viewText, 'h1Text' => $h1Text]);
?>

