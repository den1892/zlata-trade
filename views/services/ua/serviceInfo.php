<?php

/* @var $this yii\web\View */

$mainInfo = [
    'acceptance' => [
        'title' => 'Закупівля зернових та олійних культур на Півдні України і їх ціни',
        'meta_description' => 'Закупівля зернових і олійних культур: Ячмінь фуражний, Пшениця фуражна, Пшениця 2-3 класу, Ріпак, Кукурудза',
        'meta_keywords' => 'закупівля ріпаку, зерно ціна, закупівля кукурудзи, зерно купити, ячмінь фуражний',
        'h1' => 'Закупка зернових<br>та олійних культур',
        'img' => '/assets/images/services_img1.png',
    ],
    'realization' => [
        'title' => 'Ціни на реалізацію мінеральних добрив в компанії Zlata Trade',
        'meta_description' => 'Ціни на реалізацію мінеральних добрив в Zlata Trade: Купити аміачну селітру, насіння соняшнику та ін за доступними цінами',
        'meta_keywords' => 'сульфат амонію, селітра аміачна, амофос, карбамід',
        'h1' => 'Реалізація мінеральних<br>добрив',
        'img' => '/assets/images/services_img2.jpg',
    ],
    'transport' => [
        'title' => 'Послуги Залізничного та Автомобільного експедирування вантажів по Україні',
        'meta_description' => 'Zlata Trade надає послуги залізничного та автомобільного експедирування вантажів по Україні',
        'meta_keywords' => 'Послуги експедиції, автоекспедиція, ж / д експедирування',
        'h1' => 'Автопослуги<br>та залізничне експедирування',
        'img' => '/assets/images/services_img3.png',
    ],
];
$mainInfo = $mainInfo[$serviceKey];
$h1Text = 'Діяльність';

$this->title = $mainInfo['title'];
$this->registerMetaTag([
    'name' => 'description',
    'content' => $mainInfo['meta_description'],
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $mainInfo['meta_keywords'],
]);

echo $this->render('/templates/serviceInfo', ['mainInfo' => $mainInfo, 'h1Text' => $h1Text, 'services' => $services, 'serviceKey' => $serviceKey]);
