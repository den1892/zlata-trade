<?php

use yii\helpers\Url;

$this->title = 'Послуги із закупівлі, перевезення та реалізації зернових культур Zlata Trade';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Закупівля зернових та масляних культур, автопослуги та експедирування, а також реалізація мінеральних добрив в Україні',
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'Зерно закупівля, закупівля зернових культур, закупівля зерна ціна, експедирування, транспортне експедирування',
]);

?>


<?php 

$services = [
    [
        'title' => 'Закупка зернових<br>та олійних культур',
        'img' => '/rest/dist-v1.1/img/service1.png',
        'url' => Url::to(['services/service-info', 'service' => 'acceptance', 'language' => Yii::$app->language]),
    ],
    [
        'title' => 'Автопослуги<br>и ж/д експедирування',
        'img' => '/rest/dist-v1.1/img/service3.png',
        'url' => Url::to(['services/service-info', 'service' => 'transport', 'language' => Yii::$app->language]),
    ],
    [
        'title' => 'Реалізація<br>мінеральних добрив',
        'img' => '/rest/dist-v1.1/img/service2.png',
        'url' => Url::to(['services/service-info', 'service' => 'realization', 'language' => Yii::$app->language]),
    ],
];

$viewText = 'Детальніше';
$h1Text = 'Діяльність';
echo $this->render('/templates/services.php', ['services' => $services, 'viewText' => $viewText, 'h1Text' => $h1Text]);
?>

