<?php

use yii\helpers\Url;

$this->title = 'Services of purchase, transportation and realization of grain crops Zlata Trade';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Purchase of cereals and olive crops, auto services and forwarding, as well as the sale of mineral fertilizers in Ukraine',
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'Grain purchase, purchase of grain crops, purchase of grain price, forwarding, transport forwarding',
]);

?>


<?php 

$services = [
    [
        'title' => 'Grains<br>and oilseeds purchase',
        'img' => '/rest/dist-v1.1/img/service1.png',
        'url' => Url::to(['services/service-info', 'service' => 'acceptance', 'language' => Yii::$app->language]),
    ],
    [
        'title' => 'Auto services<br>and railway forwarding',
        'img' => '/rest/dist-v1.1/img/service3.png',
        'url' => Url::to(['services/service-info', 'service' => 'transport', 'language' => Yii::$app->language]),
    ],
    [
        'title' => 'Fertilizers sales',
        'img' => '/rest/dist-v1.1/img/service2.png',
        'url' => Url::to(['services/service-info', 'service' => 'realization', 'language' => Yii::$app->language]),
    ],
];

$viewText = 'View';
$h1Text = 'Activity';
echo $this->render('/templates/services.php', ['services' => $services, 'viewText' => $viewText, 'h1Text' => $h1Text]);
?>

