<?php

/* @var $this yii\web\View */

$mainInfo = [
    'acceptance' => [
        'title' => 'Purchase of grain and oilseeds in the South of Ukraine and their prices',
        'meta_description' => 'purchasing different cereals and oil crops: Barley fodder, Wheat fodder, Wheat of 2-3 classes, Rape, Corn',
        'meta_keywords' => 'grain price, corn purchase, grain to buy, feed barley',
        'h1' => 'Grains<br>and oilseeds purchase',
        'img' => '/assets/images/services_img1.png',
    ],
    'realization' => [
        'title' => 'Prices for the sale of mineral fertilizers in Zlata Trade',
        'meta_description' => 'Prices for the sale of mineral fertilizers in Zlata Trade: Buy ammonium nitrate, sunflower seeds and others at affordable prices',
        'meta_keywords' => 'Ammonium sulphate, ammonium nitrate, amofos, carbamide',
        'h1' => 'Fertilizers sales',
        'img' => '/assets/images/services_img2.jpg',
    ],
    'transport' => [
        'title' => 'Services of Railway and Road freight forwarding in Ukraine',
        'meta_description' => 'Zlata Trade provides services of railway and road freight forwarding in Ukraine',
        'meta_keywords' => 'Forwarding services, auto expedition, railway forwarding',
        'h1' => 'Auto services<br>and railway forwarding',
        'img' => '/assets/images/services_img3.png',
    ],
];
$mainInfo = $mainInfo[$serviceKey];
$h1Text = 'Activity';

$this->title = $mainInfo['title'];
$this->registerMetaTag([
    'name' => 'description',
    'content' => $mainInfo['meta_description'],
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => $mainInfo['meta_keywords'],
]);

echo $this->render('/templates/serviceInfo', ['mainInfo' => $mainInfo, 'h1Text' => $h1Text, 'services' => $services, 'serviceKey' => $serviceKey]);
