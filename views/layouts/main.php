<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\AppAsset;

AppAsset::register($this);
$this->params['bodyClass'] = '';
?>
    <?php $this->beginPage(); ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language; ?>">

    <head>
        <meta charset="<?= Yii::$app->charset; ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
        <meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1.0, user-scalable=no">
        <meta name="format-detection" content="telephone=no">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="https://zlata-trade.com/rest/assets/images/favicon.ico" type="image/x-icon">
        <?= Html::csrfMetaTags(); ?>
            <title>
                <?= Html::encode($this->title); ?>
            </title>
            <?php $this->head(); ?>
    </head>

    <body>
        <?php $this->beginBody(); ?>
       
            <?= $content; ?>

        <?php $this->endBody(); ?>
        <script type=text/javascript>
            (function (d, w, c) {
                (w[c] = w[c] || []).push(function () {
                    try {
                        w.yaCounter45710367 = new Ya.Metrika({
                            id: 45710367,
                            clickmap: true,
                            trackLinks: true,
                            accurateTrackBounce: true,
                            webvisor: true,
                            trackHash: true
                        });
                    } catch (e) {}
                });

                var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () {
                        n.parentNode.insertBefore(s, n);
                    };
                s.type = "text/javascript";
                s.async = true;
                s.src = "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/watch.js";

                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else {
                    f();
                }
            })(document, window, "yandex_metrika_callbacks");
        </script>
        <noscript>
            <div>
                <img src=https://mc.yandex.ru/watch/45710367 style="position:absolute; left:-9999px" alt=Метрика>
            </div>
        </noscript>
        <!-- /Yandex.Metrika counter -->
    </body>

    <script id="contactTopTemplate" type="text/html">
        <h4>${contacts_top.office_name}</h4>
        <p>${contacts_top.tel1}</p>
        <p>${contacts_top.tel2}</p>
        <p>${contacts_top.addr1}</p>
        <p>${contacts_top.addr2}</p>

    </script>
    <script id="contactBotTemplate" type="text/html">
        <h4>${contacts_bottom.office_name}</h4>
        <p>${contacts_bottom.tel1}</p>
        <p>${contacts_bottom.tel2}</p>
        <p>${contacts_bottom.addr1}</p>
        <p>${contacts_bottom.addr2}</p>

    </script>

    </html>
    <?php $this->endPage(); ?>