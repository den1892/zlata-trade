<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = '404';
$mainText = 'Главная';
$activityText = 'Деятельность';
$publication = 'Пуликации';
$pageNotFoundText = 'Страница не найдена';
$VisitOurOtherPagesText = 'Посетите наши другие страницы';

echo $this->render('/templates/error', [
    'mainText' => $mainText,
    'activityText' => $activityText,
    'publication' => $publication,
    'pageNotFoundText' => $pageNotFoundText, 'VisitOurOtherPagesText' => $VisitOurOtherPagesText,
]);
