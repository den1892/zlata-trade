<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = '404';
$mainText = 'Home';
$activityText = 'Activity';
$publication = 'Publication';
$pageNotFoundText = 'Page not found';
$VisitOurOtherPagesText = 'Visit our other pages';

echo $this->render('/templates/error', [
    'mainText' => $mainText,
    'activityText' => $activityText,
    'publication' => $publication,
    'pageNotFoundText' => $pageNotFoundText, 'VisitOurOtherPagesText' => $VisitOurOtherPagesText,
]);
