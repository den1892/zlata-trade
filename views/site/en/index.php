<?php


/* @var $this yii\web\View */
$this->title = 'Zlata Trade - grain trader in the South of Ukraine, official website';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'A major trader in the sale and purchase of grain, as well as mineral fertilizers in the South of Ukraine.',
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'Grain trader, grain trader Ukraine, grain trader in Bessarabia, supplier of mineral fertilizers',
]);

?>

<?php
$geodata = yii::$app->geolocation->getInfo();
$country_code = $geodata['geoplugin_countryCode'];
?>

<?php

$sliders = [
    [
        'title' => 'A rational kernel of your business',
        'desc' => 'We represent reliability, strength and stability. Born on
        earth, called to fly in the sky, like a falcon, we are catching the fair wind so high,
        that other birds just can’t get there. We are happy to take under our wing those, who
        achieve the heights in their business.',
    ],
    [
        'title' => 'Start from yourself',
        'desc' => 'The secret of our prosperity lies in the success of your
        business. You can be sure, that by growing the high quality grains, we can provide your
        business with the needed volume of services. The right approach and high technological
        potential distinguish Zlata Trade from its business rivals.',
    ],
];

$addSlide = [
    'title' => 'Commerce',
    'desc' => 'Since our foundation in 2008, we managed to become one of
    the most successful traders in the sphere of sales and purchase of grains in the South of
    Ukraine. We have improved our manufacturing technologies and gained favor with our suppliers
    thanks to our great experience. Two grain elevators allow us to work with large volumes of
    grains, and the trucks, belonging to our company, can transport the required volume of
    grains in the shortest time possible. Our activity is connected both with grains and
    fertilizers supply. All of it can be provided to you. All of it is Zlata Trade itself.',
    'buttonText' => 'Activity',
];

echo $this->render('/templates/index.php', [
    'sliders' => $sliders,
    'addSlide' => $addSlide,
]);

?>
 