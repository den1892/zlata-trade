<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

$this->title = '404';
$mainText = 'Головна';
$activityText = 'Діяльність';
$publication = 'Пулікації';
$pageNotFoundText = 'Сторінку не знайдено';
$VisitOurOtherPagesText = 'Відвідайте наші інші сторінки';

echo $this->render('/templates/error', [
    'mainText' => $mainText,
    'activityText' => $activityText,
    'publication' => $publication,
    'pageNotFoundText' => $pageNotFoundText, 'VisitOurOtherPagesText' => $VisitOurOtherPagesText,
]);
