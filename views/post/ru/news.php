<?php
/* @var $this yii\web\View */
$this->title = 'Актуальные новости зернового трейдинга на Юге Украины';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Возможность узнать о самых актуальных новостях зернового трейдинга на Юге Украины',
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'новости агробизнеса, новости в агробизнесе',
]);
?>

<?php 

$h1Text = 'Публикации';

echo $this->render('/templates/posts.php', [
    'posts' => $posts,
    'h1Text' => $h1Text,
]);

?>


  