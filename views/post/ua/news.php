<?php
/* @var $this yii\web\View */
$this->title = 'Актуальні новини зернового трейдингу на Півдні України';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'Можливість дізнатися про найактуальніші новини зернового трейдингу на Півдні України',
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'новини агробізнесу, новини в агробізнесі',
]);
?>

<?php 

$h1Text = 'Публікації';

echo $this->render('/templates/posts.php', [
    'posts' => $posts,
    'h1Text' => $h1Text,
]);

?>


  