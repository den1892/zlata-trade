<?php
/* @var $this yii\web\View */
$this->title = 'Topical news of grain trading in the South of Ukraine';
$this->registerMetaTag([
    'name' => 'description',
    'content' => 'The opportunity to learn about the most relevant news of grain trading in the South of Ukraine',
]);
$this->registerMetaTag([
    'name' => 'keywords',
    'content' => 'news of agribusiness, news in agribusiness',
]);
?>

<?php 

$h1Text = 'Publication';

echo $this->render('/templates/posts.php', [
    'posts' => $posts,
    'h1Text' => $h1Text,
]);

?>


  