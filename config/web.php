<?php


$params = require __DIR__.'/params.php';

$db = require __DIR__.'/db.php';

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log', 'gii', 'debug'],
    'language' => 'ua',

    'controllerMap' => [
        'file' => 'mdm\\upload\\FileController', // use to show or download file
    ],

    'modules' => [
        'gii' => [
            'class' => 'yii\gii\Module',
            // uncomment the following to add your IP if you are not connecting from localhost.
            'allowedIPs' => ['91.197.17.*, 127.0.0.1', '::1'],
        ],

        'debug' => [
            'class' => 'yii\debug\Module',
            // uncomment the following to add your IP if you are not connecting from localhost.
            'allowedIPs' => ['91.197.17.*, 127.0.0.1', '163.*.*.*', '::1'],
        ],
    ],

    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'baseUrl' => '',
            'cookieValidationKey' => 'mjupk0i3sfQ603peO9Cd79R7stKc_GsY',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],

        'geolocation' => [
            'class' => 'indicalabs\geolocation\Geolocation',
            'config' => [
                'provider' => 'geoplugin',
                'return_formats' => '[json]',
                'api_key' => '[YOUR_API_KEY]',
            ],
        ],

        'i18n' => [
            'translations' => [
                'common*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => '@app/messages',
                ],
            ],
        ],

        'assetManager' => [
            'bundles' => [
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                ],
            ],
        ],

        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'in-v3.mailjet.com',
                'username' => '2ad6397584741be38e9398adc4577079',
                'password' => '71e16511937e2b08132806d26177cb11',
                'port' => '587',
                'encryption' => 'tls',
            ],
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => $db,

        'response' => [
            // ...
            'formatters' => [
                \yii\web\Response::FORMAT_JSON => [
                    'class' => 'yii\web\JsonResponseFormatter',
                    'prettyPrint' => YII_DEBUG, // используем "pretty" в режиме отладки
                    'encodeOptions' => JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE,
                    // ...
                ],
            ],
        ],

        'urlManager' => [
            'class' => 'codemix\localeurls\UrlManager',
            'languages' => [
                'ua',
                'en-*',
                'ru-*',
            ],
            'enableDefaultLanguageUrlCode' => true,
            'enableLanguageDetection' => false,

            'enablePrettyUrl' => true,
            'showScriptName' => false,

            'ignoreLanguageUrlPatterns' => [
                '#^dashboard-users/#' => '#^dashboard-users/#',

                '#^post/get-all-posts#' => '#post/get-all-posts/#',
                '#^post/get-posts#' => '#post/get-posts/#',

                '#^post/create#' => '#post/create/#',
                '#^post/update#' => '#post/update/#',
                '#^post/delete#' => '#post/delete/#',

                '#^favicon#' => '#favicon/#',
                '#^favicon.ico#' => '#favicon.ico#',
                '#^robots.txt#' => '#robots.txt#',

                '#^services/create-service#' => '#services/create-service/#',
                '#^services/create-service/#' => '#services/create-service/#',
                '#^services/create#' => '#services/create-service/#',

                '#^services/update-service#' => '#services/update-service/#',
                '#^services/update-service/#' => '#services/update-service/#',
                '#^services/delete#' => '#services/delete/#',
                '#^services/get-all-services-by-category#' => '#services/get-all-services-by-category/#',

                '#^image/#' => '#image/#',
                '#^options/#' => '#options/#',

                '#^/assets/#' => '#^/assets/#',
                '#^debug#' => '#debug/#',
                '#^gii#' => '#gii/#',
            ],

            'rules' => [
                //Site actions
                '' => 'site/index',
                'GET site/index' => 'site/index',

                // ...other rules...
                [
                    //'pattern' => 'post/get-posts/<page:\d+>/<language>',
                    'pattern' => 'post/get-posts/<language>',
                    'route' => 'post/get-posts',
                    'defaults' => [
                        //'page' => 1,
                        'language' => 'ua', ],
                ],

                // Post actions
                'GET news/<page_id:\d+>' => 'post/news',
                'GET news/' => 'post/news',
                'GET post/get-all-posts/<language:\w+>' => 'post/get-all-posts',

                'GET post/get-post/<post_id:\d+>' => 'post/get-post/',
                'POST post/create' => 'post/create',
                'POST post/delete' => 'post/delete',
                'POST post/update' => 'post/update',

                // Services actions
                'GET services' => 'services/services',
                'GET services/' => 'services/services',
                'GET services/get-all-services' => 'services/get-all-services',
                'POST services/get-all-services-by-category' => 'services/get-all-services-by-category',
                'POST services/create-service' => 'services/create-service',
                'POST services/update-service' => 'services/update-service',
                'POST services/delete-service' => 'services/delete',
                'GET services/services/' => 'services/services',

                'GET services/<service:\w+>' => 'services/service-info',
                // 'GET services/realization/' => 'services/services-realization',
                // 'GET services/transport/' => 'services/services-transport',

                // Images actions
                'GET image/view/<id:\d+>' => 'image/view/',
                'POST image/delete/' => 'image/delete/',
                'POST image/create/' => 'image/create/',

                // Options actions
                'GET options/get-contacts-options/<language:\w+>' => 'options/get-contacts-options',

                'GET options/get-option-by-name/<option_name:\w+>' => 'options/get-option-by-name',
                'GET options/get-options-by-group/<options_group:\w+>' => 'options/get-options-group',

                'POST options/create-option' => 'options/create-option',
                'POST options/delete-option' => 'options/delete-option',
                'POST options/update-contacts-options' => 'options/update-contacts-options',

                //REST Controller for Users Dashboard
                ['class' => 'yii\rest\UrlRule', 'controller' => 'dashboard-users',
                    'extraPatterns' => [
                        'POST login' => 'login',
                        'POST register' => 'register',
                        'POST add -role' => 'add-role',
                        'POST edit-role' => 'edit-role',
                        'POST delete-role' => 'delete-role',
                        'GET  show-roles' => 'show-roles',
                        'POST show-users' => 'show-users',
                        'POST edit-user' => 'edit-user',
                        'POST delete-user' => 'delete-user',
                        'POST users-count' => 'users-count',
                        'POST reset-token' => 'reset-token',
                    ],
                ],

                'gii' => 'gii',
                'debug' => 'debug',
            ],
        ],
    ],
    'params' => $params,
];

return $config;
