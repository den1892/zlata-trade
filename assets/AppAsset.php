<?php
/**
 * @see http://www.yiiframework.com/
 *
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 *
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
'rest/dist-v1.1/css/app.8f4f094a730b3faf1119e1cf292ee4c9.css?vs=1.1',

      //  '/rest/css/vendor-0788427679.css',

       // '/rest/css/app-ff20275c84.css',
    ];

    public $js = [
'rest/dist-v1.1/js/manifest.2ae2e69a05c33dfc65f8.js',
'rest/dist-v1.1/js/vendor.e343439be69b00350d7d.js',
'rest/dist-v1.1/js/app.2ace9a0c9569ff319e9f.js',

        // '/rest/js/vendor-12d9909c5a.js',
        // '/rest/js/ga.js',

        // '/rest/js/app-603cd0e9b7.js',
    ];

    public $depends = [
//        'yii\web\YiiAsset',
//        'yii\bootstrap\BootstrapAsset',
    ];
}
