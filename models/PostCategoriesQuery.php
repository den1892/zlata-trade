<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[PostCategories]].
 *
 * @see PostCategories
 */
class PostCategoriesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return PostCategories[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return PostCategories|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
