<?php

namespace app\models;


use Yii;
use app\models\DashboardRoles as Role;
use yii\base\ErrorException;
use yii\db\Query;
use yii\web\Controller;
use yii\web\Response;

/**
 * This is the model class for table "dashboard_users".
 *
 * @property integer $id
 * @property string $name
 * @property string $username
 * @property string $password
 * @property string $email
 */
class DashboardUsers extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dashboard_users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'username', 'password', 'token'], 'string', 'max' => 255],
            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'username' => 'Username',
            'password' => 'Password',
            'email' => 'Email',
            'token' => 'Token',
        ];
    }

    public static function getUserId($token) {
        try {
            $query = new Query;
            $query->select('id')
                ->from('dashboard_users')
                ->where(['token' => $token]);
            $command = $query->createCommand();
            return $command->queryScalar();
        }

        catch (ErrorException $e) {
            Yii::warning("SQL Error");
            return false;
        }
    }

    public function testUser($token){

        try {
        $query = new Query;

        $query  ->select('dashboard_users.token, dashboard_roles.role_name, dashboard_users.name')
            ->from('dashboard_users')
            ->join('LEFT JOIN','dashboard_user_to_role', 'dashboard_user_to_role.user_id = dashboard_users.id')
            ->join('LEFT JOIN','dashboard_roles', 'dashboard_roles.id = dashboard_user_to_role.role_id')
            ->where(['dashboard_users.token' => $token]);

        $command = $query->createCommand();

        return  $command->queryOne();

        } catch (ErrorException $e) {

            Yii::warning("SQL Error");

            return false;
        }
    }

    public function checkRole($request){

        $post = $request->post('token');

        $test = $this->testUser($post);
        try {

            if ($test["role_name"] === 'ADMIN' || $test["role_name"] === 'MODERATOR' )
                return true;

            else return false;

        }catch (ErrorException $e){

            Yii::warning("SQL Error $e");

            return false;
        }
    }


    public function checkIfAdmin($request){

        $post = $request->post('token');

            $test = $this->testUser($post);
            try {

                if ($test["role_name"] === 'ADMIN')
                    return true;

                else return false;

            }catch (ErrorException $e){

                Yii::warning("SQL Error $e");

                return false;
            }
    }

    public function checkUserRole($request){

        $post = $request->post('token');

        if($post) {

            $test = $this->testUser($post);

            if($test)

                return $test;

            else return false;
        }

        return false;

    }
}
