<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "post_categories".
 *
 * @property integer $id
 * @property string $title
 *  * @property string $title_ua
 * @property string $title_ru
 * @property string $title_en
 * @property string $create_at
 */
class PostCategories extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'post_categories';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['create_at'], 'safe'],
            [['title'], 'string', 'max' => 255],
            [['title_ua'], 'string', 'max' => 255],
            [['title_ru'], 'string', 'max' => 255],
            [['title_en'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title(bd name)',
            'title_ua' => 'Title(ukr)',
            'title_ru' => 'Title(rus)',
            'title_en' => 'Title(en)',
            'create_at' => 'Created At',
        ];
    }

    /**
     * @inheritdoc
     * @return PostCategoriesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PostCategoriesQuery(get_called_class());
    }
}
