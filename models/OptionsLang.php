<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "options_lang".
 *
 * @property integer $id
 * @property integer $option_id
 * @property string $language
 * @property string $content
 *
 * @property Options $option
 */
class OptionsLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'options_lang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['option_id', 'language'], 'required'],
            [['option_id'], 'integer'],
            [['language'], 'string', 'max' => 6],
            [['content'], 'string', 'max' => 255],
            [['option_id'], 'exist', 'skipOnError' => true, 'targetClass' => Options::className(), 'targetAttribute' => ['option_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'option_id' => 'Option ID',
            'language' => 'Language',
            'content' => 'Content',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOption()
    {
        return $this->hasOne(Options::className(), ['id' => 'option_id']);
    }
}
