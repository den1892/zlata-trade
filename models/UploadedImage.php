<?php
/**
 * Created by PhpStorm.
 * User: atkach
 * Date: 03.08.17
 * Time: 11:39
 */

namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;
use app\models\Images;
use Yii;

class UploadedImage extends Model

{
    /**
     * @var UploadedFile
     */
    public $imageFile;
    public $upload_dir;
    public $upload_dir_rel;

    public function rules()
    {
        return [
            [['imageFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, gif'],
        ];
    }

    public function gen_new_fname() {
        return date("Ymd") . "_" . substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 8, 24);
    }

    public function upload()
    {
        if ( $this->validate() ) {
            $new_filename =  $this->gen_new_fname() . '.' . $this->imageFile->extension;
            $file_path = $this->upload_dir . $new_filename;
            $this->imageFile->saveAs($file_path);
            $image = new Images;
            $image->image_filename = $new_filename;
            $image->image_filesize = $this->imageFile->size;
            $image->image_filepath = $file_path;
            $image->image_url = $this->upload_dir_rel . $new_filename;
            $image->save();
            return $image->id;
        }
        else {
            return false;
        }
    }
}