<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "options".
 *
 * @property integer $id
 * @property string $option_name
 * @property string $option_group
 * @property string $option_value
 */
class Options extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'options';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['option_name', 'option_group'], 'required'],
            [['option_value'], 'string'],
            [['option_name', 'option_group'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'option_name' => 'Option Name',
            'option_group' => 'Option Group',
            'option_value' => 'Option Value',
        ];
    }

    public static function delete_service_options($post_id) {

    }
}
