<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[DashboardRoles]].
 *
 * @see DashboardRoles
 */
class DashboardRolesQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * @inheritdoc
     * @return DashboardRoles[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return DashboardRoles|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}
