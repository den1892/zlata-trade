<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "images_to_post".
 *
 * @property integer $id
 * @property integer $image_id
 * @property integer $post_id
 */
class ImagesToPost extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'images_to_post';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image_id', 'post_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image_id' => 'Image ID',
            'post_id' => 'Post ID',
        ];
    }
}
