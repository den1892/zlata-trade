<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dashboard_user_to_role".
 *
 * @property string $user_id
 * @property string $role_id
 */
class DashboardUserToRole extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dashboard_user_to_role';
    }


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'role_id' => 'Role ID',
        ];
    }
}
