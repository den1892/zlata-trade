<?php

namespace app\models;

use yii\db\Exception;

/**
 * This is the model class for table "posts".
 *
 * @property integer $id
 * @property string $type
 * @property integer $author_id
 * @property integer $category_id
 * @property string $updated_at
 * @property string $created_at
 */
class Posts extends \yii\db\ActiveRecord
{

    public static function tableName()
    {
        return 'posts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'author_id'], 'required'],
            [['author_id', 'category_id'], 'integer'],
            [['updated_at', 'created_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'author_id' => 'Author ID',
            'category_id' => 'Category ID',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @inheritdoc
     * @return PostsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new PostsQuery(get_called_class());
    }

    //get images for a given post using REST API or internal call
    public static function get_post_images($post_id) {
        if ($post_id) {
            $images_to_post = ImagesToPost::find()->where(['post_id' => $post_id])->all();
            $images = [];
            foreach ($images_to_post as $image) {
                $images[] = Images::find()
                                    ->select(array('id', 'image_url'))
                                    ->where(['id' => $image->image_id] )
                                    ->asArray()
                                    ->one();
            }
            return $images;
        }
    }

    public static function get_post_author($author_id) {
        return DashboardUsers::find()->select('name')->where(['id' => $author_id])->scalar();
    }

    public static function getMonthName($unixTimeStamp = false, $language = "ua") {
        // Если не задано время в UNIX, то используем текущий
        if (!$unixTimeStamp) {
            $mN = date('m');
            // Если задано определяем месяц времени
        } else {
            $mN = date('m', (int)$unixTimeStamp);
        }

        if ($language === "ru") {
            $monthAr = array(
                1 => array('Январь', 'Января'),
                2 => array('Февраль', 'Февраля'),
                3 => array('Март', 'Марта'),
                4 => array('Апрель', 'Апреля'),
                5 => array('Май', 'Мая'),
                6 => array('Июнь', 'Июня'),
                7 => array('Июль', 'Июля'),
                8 => array('Август', 'Августа'),
                9 => array('Сентябрь', 'Сентября'),
                10 => array('Октябрь', 'Октября'),
                11 => array('Ноябрь', 'Ноября'),
                12 => array('Декабрь', 'Декабря')
            );
        }

        else if ($language === "ua") {
            $monthAr = array(
                1 => array('Січень', 'Січня'),
                2 => array('Лютий', 'Лютого'),
                3 => array('Березень', 'Березня'),
                4 => array('Квітень', 'Квітня'),
                5 => array('Травень', 'Травня'),
                6 => array('Червень', 'Червня'),
                7 => array('Липень', 'Липня'),
                8 => array('Серпень', 'Серпня'),
                9 => array('Вересень', 'Вересня'),
                10 => array('Жовтень', 'Жовтня'),
                11 => array('Листопад', 'Листопада'),
                12 => array('Грудень', 'Грудня')
            );
        }

        else  {
            $monthAr = array(
                1 => array('January', 'January'),
                2 => array('February', 'February'),
                3 => array('March', 'March'),
                4 => array('April', 'April'),
                5 => array('May', 'May'),
                6 => array('June', 'June'),
                7 => array('July', 'July'),
                8 => array('August', 'August'),
                9 => array('September', 'September'),
                10=> array('October', 'October'),
                11=> array('November', 'November'),
                12=> array('December', 'December')
            );
        }

        return $monthAr[(int)$mN][1];
    }

    public static function date_format($date, $language = "ua") {
        $day = date('d', (int) $date);
        $month = Posts::getMonthName($date, $language);
        $year = date('Y', (int) $date);
        return "$day $month $year";
    }

    //delete post images (internal call)
    public static function delete_post_images($post_id) {
        try {
            $images_to_post = ImagesToPost::find()->where(['post_id'=>$post_id])->all();
            foreach ($images_to_post as $image_to_post) {
                $image_to_post->delete();
            }
            return true;
        }
        catch (Exception $exception) {
            return false;
        }
    }

    public static function set_post_images($post_id, $images) {
        if (empty($images) || empty($post_id)) {
            return false;
        }
        foreach ($images as $image) {
            $db_img = new ImagesToPost;
            $db_img->post_id = $post_id;
            $db_img->image_id = $image['id'];
            $db_img->save;
        }
        return true;
    }

    public static function create_post_translation($post_id, $language, $content_params=array() ) {
        $new_post_translation = new PostLang();
        $new_post_translation->post_id = $post_id;
        $new_post_translation->language = $language;
        $new_post_translation->content = isset($content_params['content']) ? $content_params['content'] : "";
        $new_post_translation->excerpt = isset($content_params['excerpt']) ? $content_params['excerpt'] : "";
        $new_post_translation->title = isset($content_params['title']) ? $content_params['title'] : "";
        $new_post_translation->title_ua = isset($content_params['title_ua']) ? $content_params['title_ua'] : "";
        $new_post_translation->save();
        return $new_post_translation;
    }

    public static function delete_post_translations($post_id) {
        $translations = PostLang::findAll(['post_id' => $post_id]);
        foreach ($translations as $translation) {
            $translation->delete();
        }
    }

    public static function lang_format( $post ) {
        if (empty($post['title'])) {
            $post['title'] = $post['title_ua'];
        }
        return $post;
    }
}
