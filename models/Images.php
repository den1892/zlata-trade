<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "images".
 *
 * @property integer $id
 * @property string $image_filename
 * @property string $image_filepath
 * @property string $image_url
 * @property integer $image_filesize
 * @property string $created_at
 */
class Images extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'images';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image_filename', 'image_url'], 'required'],
            [['image_filesize'], 'integer'],
            [['created_at'], 'safe'],
            [['image_filename', 'image_url'], 'string', 'max' => 255],
            [['image_filepath'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'image_filename' => 'Image Filename',
            'image_url' => 'Image Url',
            'image_filesize' => 'Image Filesize',
            'image_filepath' => 'Image Filepath',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @inheritdoc
     * @return ImagesQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ImagesQuery(get_called_class());
    }
}
