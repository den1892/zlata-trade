<?php

namespace app\models;

use Yii;
use yii\db\Exception;

/**
 * This is the model class for table "postLang".
 *
 * @property integer $id
 * @property integer $post_id
 * @property string $language
 * @property string $title
 * @property string $title_ua
 * @property string $excerpt
 * @property string $content
 */
class PostLang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'postLang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['post_id', 'language'], 'required'],
            [['post_id'], 'integer'],
            [['content'], 'string'],
            [['title_ua'], 'string', 'max' => 128],
            [['language'], 'string', 'max' => 6],
            [['title', 'excerpt'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'post_id' => 'Post ID',
            'language' => 'Language',
            'title' => 'Title',
            'title_ua' => 'Title_UA',
            'excerpt' => 'Excerpt',
            'content' => 'Content',
        ];
    }

    public static function find_translation($post_id, $language) {
        return PostLang::findOne(['post_id' => $post_id, 'language' => $language]);
    }
}
