<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "dashboard_roles".
 *
 * @property integer $id
 * @property string $name
 * @property string $permissions
 */
class DashboardRoles extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'dashboard_roles';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['role_name', 'permissions'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'role_name' => 'Role_Name',
            'permissions' => 'Permissions',
        ];
    }

    /**
     * @inheritdoc
     * @return DashboardRolesQuery the active query used by this AR class.
     */
  //  public static function find()
  //  {
  //      return new DashboardRolesQuery(get_called_class());
  //  }

}
