<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "options_to_posts".
 *
 * @property integer $id
 * @property integer $option_id
 * @property integer $post_id
 */
class OptionsToPosts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'options_to_posts';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['option_id', 'post_id'], 'required'],
            [['option_id', 'post_id'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'option_id' => 'Option ID',
            'post_id' => 'Post ID',
        ];
    }
}
