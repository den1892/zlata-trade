<?php 

use yii\helpers\Url;

?>
<div class=sidenav_header>
    <div class="row sidenav_header_wrapper">
        <div class="col d-flex">
            <span class=dropdown-el>
                <?php if ($languages) :?>
                <?php foreach ($languages as $k => $langInfo) :?>
                
                <input <?= Yii::$app->language == $langInfo['url'] ? 'checked' : ''; ?> type=radio name=dropdown data-url="<?=Url::to([$route, 'language' => $langInfo['url']]); ?>" value='' id=select_<?=$k; ?>>
                <label for=select_<?=$k; ?>><?=$langInfo['label']; ?></label>
               
                <?php endforeach; ?>
                <?php endif; ?>
            </span>
        </div>
        <div class="col d-flex justify-content-end align-items-center">
            <div class=button-close-main>
                <a class=btn_close href=javascript:void(0)></a>
            </div>
        </div>
    </div>
</div>