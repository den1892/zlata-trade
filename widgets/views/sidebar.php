<?php 

use yii\helpers\Url;

?>

<div class="sidenav px-5">
    <div class=sidenav_content>
        <?= \app\widgets\LanguageSelect::widget(['route' => $route]); ?>
            <div class=sidenav_links>
                <div class="row sidenav_links_wrapper">
                    <div class="links_overflow d-none d-lg-block"></div>
                    <ul class=col>
                        <li class=d-flex>
                            <a class=pageSwitcher href=javascript:void(0) data-url=<?=Url::to(['/', 'language' => Yii::$app->language]); ?>>
                                <?=$translates['urlTextMain']; ?>
                            </a>
                        </li>
                        <li class=d-flex>
                            <a class=pageSwitcher href=javascript:void(0) data-url=<?=Url::to(['services/services', 'language' => Yii::$app->language]); ?>>
                                <?=$translates['urlTextServices']; ?>
                            </a>
                        </li>
                        <li class=d-flex>
                            <a class=pageSwitcher href=javascript:void(0) data-url=<?=Url::to(['post/news', 'language' => Yii::$app->language]); ?>>
                                <?=$translates['urlTextNews']; ?>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="sidenav_contacts d-none d-lg-block">
                <div class="row sidenav_contacts_wrapper text-right mb-5">
                    <?=$translates['contacts1']; ?>
                </div>
                <div class="row sidenav_contacts_wrapper text-right mb-5">
                    <?=$translates['contacts2']; ?>
                </div>
            </div>
            <div class="sidenav_logo d-block d-lg-none">
                <img src=/rest/dist-v1.1/img/logo_back.png alt="">
            </div>
    </div>
</div>
<div class=sidenav_overlay></div>
<div class="sidenav__contact d-flex d-lg-none px-5">
    <div class=sidenav__contact_content>
        <div class=sidenav__contact_header>
            <div class="row sidenav__contact_header_wrapper">
                <div class="col d-flex justify-content-start align-items-center">
                    <div class=button-close-main>
                        <a class=btn_close__contact href=javascript:void(0)></a>
                    </div>
                </div>
            </div>
        </div>
        <div class=sidenav__contact_contacts>
            <div class="row sidenav__contact_contacts_wrapper text-center mb-5">
                <?=$translates['contacts1']; ?>
            </div>
            <div class="row sidenav__contact_contacts_wrapper text-center mb-5">
                <?=$translates['contacts2']; ?>
            </div>
        </div>
        <div class=sidenav__contact_logo>
            <img src=/rest/dist-v1.1/img/logo_back.png alt="">
        </div>
    </div>
</div>