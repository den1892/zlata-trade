<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\widgets;

use Yii;
use yii\base\Widget;

/**
 * Description of Form.
 *
 * @author programmer_5
 */
class Sidebar extends Widget
{
    public $translates = [
        'ua' => [
            'contacts1' => '<h4 class=col-12>Підрозділ Сарата</h4>'.
                           '<p class=col-12>+38 (067) 158 21 79</p>'.
                           '<p class=col-12>Одеська обл., смт Сарата</p>'.
                           '<p class=col-12>вул. Промзона 14</p>',
            'contacts2' => '<h4 class=col-12>Підрозділ Хмельник</h4>'.
                           '<p class=col-12>+38 (067) 519 06 84</p>'.
                           '<p class=col-12>Вінницька обл.., м. Хмельник</p>'.
                           '<p class=col-12>вул. Привокзальна 31</p>',
            'urlTextMain' => 'Головна',
            'urlTextServices' => 'Діяльність',
            'urlTextNews' => 'Публікації',
        ],
        'ru' => [
            'contacts1' => '<h4 class=col-12>Подразделение Сарата</h4>'.
                           '<p class=col-12>+38 (067) 158 21 79</p>'.
                           '<p class=col-12>Одесская обл., пгт Сарата</p>'.
                           '<p class=col-12>ул. Промзона 14</p>',
            'contacts2' => '<h4 class=col-12>Подразделение Хмельник</h4>'.
                           '<p class=col-12>+38 (067) 519 06 84</p>'.
                           '<p class=col-12>Винницкая обл., г. Хмельник</p>'.
                           '<p class=col-12>ул Привокзальная 31</p>',
            'urlTextMain' => 'Главная',
            'urlTextServices' => 'Деятельность',
            'urlTextNews' => 'Публикации',
        ],
        'en' => [
            'contacts1' => '<h4 class=col-12>Sarata Department</h4>'.
                           '<p class=col-12>+38 (067) 158 21 79</p>'.
                           '<p class=col-12>Odesska oblast, Sarata town</p>'.
                           '<p class=col-12>Promzona Str. 14</p>',
            'contacts2' => '<h4 class=col-12>Khmelnyk Departament</h4>'.
                           '<p class=col-12>+38 (067) 519 06 84</p>'.
                           '<p class=col-12>Vinnytska oblast, Khmelnyk city</p>'.
                           '<p class=col-12>Pryvokzalna Str. 31</p>',
            'urlTextMain' => 'Home',
            'urlTextServices' => 'Activity',
            'urlTextNews' => 'Publications',
        ],
    ];

    public $route = null;

    public function init()
    {
        $this->translates = isset($this->translates[Yii::$app->language]) ? $this->translates[Yii::$app->language] : $this->translates['ua'];
    }

    public function run()
    {
        return $this->render('sidebar', [
            'translates' => $this->translates,
            'route' => $this->route,
        ]);
    }
}
