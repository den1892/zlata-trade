<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\widgets;

use Yii;
use yii\base\Widget;

/**
 * Description of Form.
 *
 * @author programmer_5
 */
class LanguageSelect extends Widget
{
    public $languages = [
        ['label' => 'UA', 'url' => 'ua'],
        ['label' => 'EN', 'url' => 'en'],
        ['label' => 'RU', 'url' => 'ru'],
        // 'ua',
        // 'en-*',
        // 'ru-*',
    ];

    public $route = null;

    public function init()
    {
        if (!$this->route) {
            $this->route = Yii::$app->controller->route;
        }
    }

    public function run()
    {
        return $this->render('languageSelect', [
                      'languages' => $this->languages,
                      'route' => $this->route,
            ]);
    }
}
