<?php
/**
 * Created by PhpStorm.
 * User: atkach
 * Date: 16.08.17
 * Time: 15:52.
 */

namespace app\controllers;

use app\models\PostCategories;
use Yii;
use app\models\Posts;
use app\models\PostLang;
use app\models\DashboardUsers;
use app\models\OptionsToPosts;
use app\models\Options;
use yii\db\Exception;
use yii\web\Controller;
use yii\web\Response;

class ServicesController extends Controller
{
    public $enableCsrfValidation = false;

    //get all posts for admin page
    public function actionGetAllServices()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $posts = Posts::find()->where(['type' => 'service'])->asArray()->all();

        return $this->format_services($posts)['rows'];
    }

    //get all posts for front-end
    public function actionGetAllServicesFront()
    {
        $posts = $this->format_services(
            Posts::find()
                ->where(['type' => 'service'])
                ->asArray()
                ->all(),
            true);

        return $posts;
    }

    public function actionCreateService()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = new DashboardUsers();
        $request = Yii::$app->request;
        $token = $request->post('token');

        if ($user->checkRole($request)) {
            $user_id = DashboardUsers::getUserId($token);
            $category_id = PostCategories::findOne(['title' => $request->post('service_category')])->id;

            if (!$category_id) {
                Yii::$app->response->statusCode = 500;

                return array('status' => 'error', 'message' => array('service category is missing'));
            }

            $price = ($request->post('price')) ? $request->post('price') : 0;

            $new_post = new Posts();
            $new_post->author_id = $user_id;
            $new_post->category_id = $category_id;
            $new_post->type = 'service';
            $new_post->updated_at = date('Y-m-d H:i:s');

            $content_params['content'] = '';
            $content_params['excerpt'] = '';
            $content_params['title'] = $request->post('title');
            $for_empty_params['title_ua'] = $request->post('title');

            try {
                $new_post->save();
                Posts::create_post_translation($new_post->id, 'ua', $content_params);
                Posts::create_post_translation($new_post->id, 'ru', $for_empty_params);
                Posts::create_post_translation($new_post->id, 'en', $for_empty_params);
                $this->save_price_in_options($price, 'price', $new_post->id);

                return array('status' => 'ok', 'message' => 'done', 'post_id' => $new_post->id);
            } catch (\yii\base\Exception $errors) {
                Yii::$app->response->statusCode = 500;

                return array('status' => 'error', 'message' => array($errors));
            }
        } else {
            Yii::$app->response->statusCode = 403;

            return array('status' => 'error', 'message' => "you don't have permissions to create posts");
        }
    }

    public function actionUpdateService()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = new DashboardUsers();
        $request = Yii::$app->request;
        $post_id = $request->post('post_id');
        $post = Posts::findOne(['id' => $post_id]);
        if (!$post) {
            Yii::$app->response->statusCode = 406;

            return array('status' => 'error', 'message' => 'post not found');
        }

        $price = $request->post('price');

        if ($user->checkRole($request) && $post) {
            $translation = PostLang::find()
                ->where(['post_id' => $post_id])
                ->andWhere(['language' => $request->post('language')])
                ->one();
            if (!$translation) {
                $translation = new PostLang();
                $translation->post_id = $post_id;
            }
            $translation->title = $request->post('title');

            try {
                $post->save();
                $translation->save();
                if ($price) {
                    $this->update_price_in_options($price, 'price', $post_id);
                }

                return array('status' => 'ok', 'message' => 'ok');
            } catch (Exception $errors) {
                Yii::$app->response->statusCode = 500;

                return array('status' => 'error', 'message' => array($errors));
            }
        }
        Yii::$app->response->statusCode = 403;

        return array('status' => 'error', 'message' => "you don't have permissions to edit posts");
    }

    public function save_price_in_options($price, $price_type, $post_id)
    {
        if ($price && $price_type && $post_id) {
            $option = new Options();
            $option->option_name = $price_type;
            $option->option_group = 'prices';
            $option->option_value = $price;
            if ($option->save()) {
                $option_to_post = new OptionsToPosts();
                $option_to_post->option_id = $option->id;
                $option_to_post->post_id = $post_id;
                if ($option_to_post->save()) {
                    return true;
                }
            }
        }

        return false;
    }

    public function update_price_in_options($new_price, $price_type, $post_id)
    {
        $price_option_id = Options::find()
            ->select(['options.id'])
            ->join('LEFT JOIN', 'options_to_posts', 'options.id = options_to_posts.option_id')
            ->where(['options_to_posts.post_id' => $post_id])
            ->andWhere(['options.option_name' => $price_type])
            ->andWhere(['options.option_group' => 'prices'])
            ->scalar();
        try {
            Yii::$app->db->createCommand()
                ->update('options', ['option_value' => $new_price], ['id' => $price_option_id])
                ->execute();

            return true;
        } catch (Exception $error) {
            return false;
        }
    }

    // get all services of the specified service category
    public function actionGetAllServicesByCategory($in_json = true)
    {
        if ($in_json) {
            Yii::$app->response->format = Response::FORMAT_JSON;
        }
        $request = Yii::$app->request;
        $language = !empty($request->post('language')) ? $request->post('language') : 'ua';

        $service_category = PostCategories::find()
                            ->select('*')
                            ->where(['title' => $request->post('category')])
                            ->one();

        if ($service_category) {
            $services = Posts::find()
                        ->select(['post_id', 'created_at', 'updated_at', 'title', 'title_ua', 'author_id', 'language'])
                        ->leftJoin('postLang', '`postLang`.`post_id` = `posts`.`id`')
                        ->where(['type' => 'service'])
                        ->andWhere(['category_id' => PostCategories::findOne(['title' => $service_category])->id])
                        ->andWhere(['language' => $language])
                        ->asArray()
                        ->all();

            return $this->format_services($services);
        }
        Yii::$app->response->statusCode = 500;

        return array('status' => 'error', 'message' => 'category not found');
    }

    public function format_services($posts = array(), $images_only = false)
    {
        $formatted_posts = array();
        if ($images_only) {
            foreach ($posts as $post) {
                if (Posts::get_post_images($post['post_id'])) {
                    $post['images'] = Posts::get_post_images($post['post_id']);
                    $post['price'] = $this->get_service_price($post['post_id']);
                    $post = Posts::lang_format($post);
                    $formatted_posts[] = $post;
                }
            }

            return $formatted_posts;
        } else {
            foreach ($posts as $post) {
                $post['images'] = Posts::get_post_images($post['post_id']);
                $post['price'] = $this->get_service_price($post['post_id']);
                $post = Posts::lang_format($post);
                $formatted_posts[] = $post;
            }

            return $formatted_posts;
        }
    }

    public function get_service_price($post_id)
    {
        return Options::find()
            ->select(['option_value'])
            ->join('LEFT JOIN', 'options_to_posts', 'options.id = options_to_posts.option_id')
            ->where(['options_to_posts.post_id' => $post_id])
            ->andWhere(['option_name' => 'price'])
            ->scalar();
    }

    public function actionDelete()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $user = new DashboardUsers();
        $request = Yii::$app->request;
        $post_id = $request->post('post_id');
        $language = (Yii::$app->request->post('language') ? Yii::$app->request->post('language') : 'ua');
        $post_translation = PostLang::find_translation($post_id, $language);

        if ($user->checkRole($request) && Posts::findOne(['id' => $post_id]) && $post_translation) {
            if ($language === 'ua') {
                $this->delete_service_completely($post_id);

                return array('status' => 'ok', 'message' => 'done');
            } else {
                $post_translation->delete();

                return array('status' => 'ok', 'message' => 'done');
            }
        }
        Yii::$app->response->statusCode = 500;

        return array('status' => 'error', 'message' => 'error');
    }

    public function delete_service_completely($post_id)
    {
        $post = Posts::findOne(['id' => $post_id]);
        Posts::delete_post_translations($post_id);
        Posts::delete_post_images($post_id);
        $this->delete_related_prices($post_id);
        $post->delete();
    }

    public function delete_related_prices($post_id)
    {
        $price_option_rel = OptionsToPosts::find()->where(['post_id' => $post_id])->one();
        $price_option = Options::find()->where(['id' => $price_option_rel->option_id])->one();
        try {
            $price_option_rel->delete();
            $price_option->delete();

            return true;
        } catch (Exception $error) {
            return false;
        }
    }

    // Template action for services
    public function actionServices()
    {
        $language = substr(Yii::$app->language, 0, 2);

        return $this->render("$language/services");
    }

    public function actionServicesAcceptance()
    {
        $language = substr(Yii::$app->language, 0, 2);
        $services = $this->format_services(Posts::find()
            ->select(['title',  'title_ua', 'post_id'])
            ->leftJoin('postLang', '`postLang`.`post_id` = `posts`.`id`')
            ->where(['type' => 'service'])
            ->andWhere(['category_id' => PostCategories::findOne(['title' => 'acceptance'])->id])
            ->andWhere(['language' => $language])
            ->asArray()
            ->all()
        );

        return $this->render("$language/services-acceptance", ['services' => $services]);
    }

    public function actionServicesRealization()
    {
        $language = substr(Yii::$app->language, 0, 2);
        $services = $this->format_services(
            Posts::find()
            ->select(['title', 'title_ua', 'post_id'])
            ->leftJoin('postLang', '`postLang`.`post_id` = `posts`.`id`')
            ->where(['type' => 'service'])
            ->andWhere(['category_id' => PostCategories::findOne(['title' => 'realization'])->id])
            ->andWhere(['language' => $language])
            ->asArray()
            ->all()
        );

        return $this->render("$language/services-realization", ['services' => $services]);
    }

    public function actionServicesTransport()
    {
        $language = substr(Yii::$app->language, 0, 2);
        $services = $this->format_services(
            Posts::find()
                ->select(['title', 'title_ua', 'post_id'])
                ->leftJoin('postLang', '`postLang`.`post_id` = `posts`.`id`')
                ->where(['type' => 'service'])
                ->andWhere(['category_id' => PostCategories::findOne(['title' => 'transport'])->id])
                ->andWhere(['language' => $language])
                ->asArray()
                ->all()
        );

        return $this->render("$language/services-transport", ['services' => $services]);
    }

    public function actionServiceInfo($service = null)
    {
        //  dump($service, 1);
        $language = substr(Yii::$app->language, 0, 2);
        $postCategories = PostCategories::findOne(['title' => $service]);
        $services = $this->format_services(
            Posts::find()
                ->select(['title', 'title_ua', 'post_id'])
                ->leftJoin('postLang', '`postLang`.`post_id` = `posts`.`id`')
                ->where(['type' => 'service'])
                ->andWhere(['category_id' => $postCategories ? $postCategories->id : null])
                ->andWhere(['language' => $language])
                ->asArray()
                ->all()
        );
        if (!$services) {
            throw new \yii\web\HttpException(404);
        }

        return $this->render("$language/serviceInfo", ['services' => $services, 'serviceKey' => $service]);
    }
}
