<?php

namespace app\controllers;
use app\models\ImagesToPost;
use Yii;
use app\models\Images;
use app\models\Posts;
use app\models\DashboardUsers;
use yii\db\Exception;
use yii\web\UploadedFile;
use app\models\UploadedImage;

class ImageController extends \yii\web\Controller
{
    public $enableCsrfValidation = false;

    #view an image by its id
    public function actionView() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $img_id = $request->get('img_id');
        $image = Images::find()->where(['id' => $img_id])->one();
        if ($image) {
            return $image->image_url;
        }
        else {
            return array("status" => "error", "message" => "image not found");
        }
    }

    public function get_image_id_from_url($img_url) {
        return Images::find()->where(['image_url' => $img_url])->one();
    }

    #delete an image. Provide yout token and img_id
    public function actionDelete() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $user = new DashboardUsers();
        $request = Yii::$app->request;

        if ( $user->checkRole($request) ) {
            $img_id = $request->post('img_id');
            $img_url = $request->post('img_url');
            if ($img_id) {
                $image = Images::find()->where(['id' => $img_id])->one();
            }
            else if ($img_url) {
                $image_id = $this->get_image_id_from_url($img_url);
                $image = Images::find()->where(['id' => $image_id])->one();
            }
            if ($image) {
                unlink($image->image_filepath);
                $image->delete();
                ImagesToPost::find()->where('img_id'===$img_id)->orWhere('image_url'===$img_url)->one()->delete();
                return array("status" => "ok", "message" => "image deleted");
            }
            else {
                return array("status" => "error", "message" => "image not found");
            }
        }
        else {
            Yii::$app->response->statusCode = 403;
            return array("status" => "error", "message" => "you don't have permissions to delete images");
        }
    }

    #Create image from POST request. Provide token and file.
    public function actionCreate()
    {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $user = new DashboardUsers();
        $request = Yii::$app->request;
        $post_id = $request->post('post_id');

        $post = Posts::find()->where(['id'=>$post_id])->one();

        if ( empty($post_id) || empty($_FILES["imageFile"] ) ) {
            Yii::$app->response->statusCode = 406;
            return array("status" => "error", "message" => "no post id specified or empty image");
        }

        if ($post->type === "service") {
            Posts::delete_post_images($post_id);
        }

        if ( count(Posts::get_post_images($post_id)) >= 3 && $post->type!=='service') {
            Yii::$app->response->statusCode = 406;
            return array("status" => "error", "message" => "post already has 3 images, remove one the old ones", "data"=>$post_img_count);
        }

        if ($user->checkRole($request)) {
            $model = new UploadedImage();
            $model->upload_dir = Yii::$app->basePath . '/uploads/';
            $model->upload_dir_rel = '/uploads/';
            $uploads = $_FILES["imageFile"];

            $model->imageFile = new UploadedFile( [
                'name' => $uploads['name'],
                'tempName' => $uploads['tmp_name'],
                'type' => $uploads['type'],
                'size' => $uploads['size'],
                'error' => $uploads['error'],
            ]);

            $image_id = $model->upload();
            if ( $image_id && $this->set_image_to_post($image_id, $post_id) ) {
                return array("status" => "ok", "message" => "image uploaded succesfully", "img_id"=>$image_id);
            }
            Yii::$app->response->statusCode = 500;
            return array("status" => "error", "message" => "error while uploading image");
        }
        Yii::$app->response->statusCode = 403;
        return array("status" => "error", "message" => "you don't have permissions to create images");
    }

    //helper internal function
    public function set_image_to_post($image_id, $post_id) {
        if ($image_id && $post_id) {
            $images_to_posts = new ImagesToPost();
            $images_to_posts->post_id = $post_id;
            $images_to_posts->image_id = $image_id;
            return $images_to_posts->save();
        }
        return false;
    }
}
