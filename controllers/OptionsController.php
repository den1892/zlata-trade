<?php

namespace app\controllers;

use Yii;
use app\models\OptionsLang;
use app\models\Options;
use app\models\DashboardUsers;
use yii\db\Exception;

class OptionsController extends \yii\web\Controller
{
    public $enableCsrfValidation = false;

    public function actionUpdateOption() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $user = new DashboardUsers();
        $request = Yii::$app->request;
        $language = !empty(Yii::$app->request->post('language')) ? Yii::$app->request->post('language') : 'ua';

        if ($user->checkRole($request)) {
            $option_id = $request->post('option_id');
            $option_name = $request->post('option_name');
            $option = Options::find()
                        ->where(['id' => $option_id])
                        ->orWhere(['option_name'=>$option_name])
                        ->one();
            $option->option_name = $request->post('option_name');
            $option->option_group = $request->post('option_group');

            if ( $option->save() ) {
                $this->insert_or_update_translation($option_id, $request->post('option_value'), $language);
                return array("status" => "ok", "message" => "option updated");
            }
            Yii::$app->response->statusCode = 500;
            return array("status" => "error", "message" => "error saving option");
        }
        Yii::$app->response->statusCode = 403;
        return array("status" => "error", "message" => "you don't have permissions to update options");
    }

    public function actionCreateOption() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $user = new DashboardUsers();

        if ($user->checkRole(Yii::$app->request)) {
            $option = new Options();
            $option->option_name = Yii::$app->request->post('option_name');
            $option->option_group = Yii::$app->request->post('option_group');
            if ( $this->check_if_exists($option->option_name, $option->option_group)) {
                Yii::$app->response->statusCode = 406;
                return array("status" => "error", "message" => "option already exists");
            }
            try {
                $option_saved = $option->save();
                $value = Yii::$app->request->post('option_value');
                if ($option_saved) {
                    $this->insert_or_update_translation($option->id, $value, "ua");
                    $this->insert_or_update_translation($option->id, "", "en");
                    $this->insert_or_update_translation($option->id, "", "ru");
                    return array("status" => "ok", "message" => "option added");
                }
                else {
                    Yii::$app->response->statusCode = 500;
                    return array("status" => "error", "message" => array($option->errors, $option_lang->errors));
                }
            }
            catch (Exception $error) {
                Yii::$app->response->statusCode = 500;
                return array("status" => "error", "message" => "error saving option $error");
            }

        }
        Yii::$app->response->statusCode = 403;
        return array("status" => "error", "message" => "you don't have permissions to create options");
    }

    public function insert_or_update_translation($option_id, $content = "", $language) {
        $option_translation = OptionsLang::find()
            ->where(['option_id' => $option_id])
            ->andWhere(['language' => $language])
            ->one();

        if (!$option_translation) {
            $option_translation = new OptionsLang();
            $option_translation->option_id = $option_id;
            $option_translation->language = $language;
        }
        $option_translation->content = $content;
        return $option_translation->save();
    }

    public function actionDeleteOption() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $user = new DashboardUsers();
        $request = Yii::$app->request;
        $option = Options::find()
                            ->where(['option_name'=>$request->post('option_name')])
                            ->andWhere(['option_group'=>$request->post('option_group')])
                            ->one();
        if ( $user->checkRole($request) && $option ) {
            $this->delete_opt_translations($option->id);
            $option->delete();
            return array("status" => "ok", "message" => "done");
        }
        else {
            Yii::$app->response->statusCode = 500;
            return array("status"=>"error", "message"=>"no such option or access error");
        }
    }

    public function delete_opt_translations($option_id) {
        $translations = OptionsLang::findAll(['option_id' => $option_id]);
        foreach ($translations as $translation) {
            $translation->delete();
        }
    }

    public function actionUpdateContactsOptions() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $user = new DashboardUsers();
        $request = Yii::$app->request;
        $language = !empty(Yii::$app->request->post('language')) ? Yii::$app->request->post('language') : 'ua';
        $options = $request->post('contacts');
        $option_group = $request->post('contacts_group');

        if ($user->checkRole(Yii::$app->request)){
            try {
                foreach ($options as $option_key => $option_value) {
                    $option_id = $this->create_option($option_key, $option_group);
                    $this->insert_or_update_translation($option_id, $option_value, $language);
                }
                return array("status" => "ok", "message" => "done", "data"=>'');
            }
            catch (Exception $e) {
                return array("status" => "error", "message" => "done", "data"=>$e);
            }
        }
        else {
            Yii::$app->response->statusCode = 403;
            return array("status" => "error", "message" => "you don't have permissions to update contacts options");
        }
    }

    public function set_empty_translations($option_id) {
        $this->insert_or_update_translation($option_id, "", "ua");
        $this->insert_or_update_translation($option_id, "", "ru");
        $this->insert_or_update_translation($option_id, "", "en");
    }

    public function create_option($option_key, $option_group) {
        if ($this->check_if_exists($option_key, $option_group)) {
            $option = Options::find()
                                ->where(['option_name'=>$option_key])
                                ->andWhere(['option_group'=>$option_group])
                                ->one();
            $option->option_group = $option_group;
            $option->save();
            return $option->id;
        }
        else {
            $option = new Options();
            $option->option_name = $option_key;
            $option->option_group = $option_group;
            $option->save();
            $this->set_empty_translations($option->id);
            return $option->id;
        }
    }

    public function actionGetContactsOptions() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $language = !empty(Yii::$app->request->get('language')) ? Yii::$app->request->get('language') : 'ua';
        $cont_top = Options::find()
            ->select('content, option_name')
            ->join('LEFT JOIN', 'options_lang', 'options.id = options_lang.option_id')
            ->where(['option_group'=>'contacts_top'])
            ->andWhere(['language'=>$language])
            ->asArray()
            ->all();
        $cont_bottom = Options::find()
            ->select('content, option_name')
            ->join('LEFT JOIN', 'options_lang', 'options.id = options_lang.option_id')
            ->where(['option_group'=>'contacts_bottom'])
            ->andWhere(['language'=>$language])
            ->asArray()
            ->all();
        return array("contacts_top" => $this->format_options($cont_top),
                    "contacts_bottom" => $this->format_options($cont_bottom)
                    );
    }

    public function format_options($options = array()) {
        $formatted_options = array();
        foreach ($options as $option) {
            $formatted_options[$option['option_name']] = $option['content'];
        }
        return $formatted_options;
    }

    public function actionGetOptionByName() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $request = Yii::$app->request;
        $option_name  = Yii::$app->request->get('option_name');
        $language = $request->get('language');
        if (empty($language)) {
            $language = "ua";
        }
        return Options::find()
            ->select('option_name, content, option_group')
            ->join('LEFT JOIN', 'options_lang', 'options.id = options_lang.option_id')
            ->where(['option_name'=>$option_name])
            ->andWhere(['language'=>$language])
            ->asArray()
            ->all();
    }

    public function actionGetOptionsGroup() {
        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $language = !empty(Yii::$app->request->get('language')) ? Yii::$app->request->get('language') : 'ua';
        return Options::find()
            ->select('option_name, content, option_group')
            ->join('LEFT JOIN', 'options_lang', 'options.id = options_lang.option_id')
            ->where(['option_group'=>Yii::$app->request->get('options_group')])
            ->andWhere(['language'=>$language])
            ->asArray()
            ->all();
    }

    public function check_if_exists($option_name, $option_group) {
            if (Options::find()
                ->where(['option_name'=>$option_name])
                ->one() )
            return true;
    }
}
