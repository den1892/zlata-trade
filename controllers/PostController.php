<?php

namespace app\controllers;

use Yii;
use app\models\Posts;
use app\models\PostLang;
use app\models\DashboardUsers;
use yii\web\Controller;
use yii\data\Pagination;

/**
 * PostController implements the CRUD actions for Posts model.
 */
class PostController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public $enableCsrfValidation = false;

    //get all posts for admin page
    public function actionGetAllPosts()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $language = Yii::$app->request->get('language') ? Yii::$app->request->get('language') : 'ua';
        $posts = Posts::find()
            ->select('*')
            ->leftJoin('postLang', '`postLang`.`post_id` = `posts`.`id`')
            ->where(['type' => 'post'])
            ->andWhere(['language' => $language])
            ->asArray()
            ->all();

        return $this->format_posts($posts)['rows'];
    }

    //format posts for showing them on front-end
    public function format_posts($posts = array(), $images_only = false, $language = 'ua')
    {
        $formatted_posts = array();
        if ($images_only) {
            foreach ($posts as $post) {
                $post_id = $post['post_id'];
                if (Posts::get_post_images($post_id)) {
                    $post['images'] = Posts::get_post_images($post_id);
                } else {
                    $post['images'][0] = array('image_url' => '/uploads/news-placeholder.png');
                }
                $post['author_name'] = Posts::get_post_author($post['author_id']);
                $post['created_at'] = Posts::date_format(strtotime($post['created_at']), $language);
                $post['updated_at'] = Posts::date_format(strtotime($post['updated_at']), $language);
                $formatted_posts[] = $post;
            }
            $formatted_posts = array_reverse($formatted_posts, true);

            return $formatted_posts;
        } else {
            foreach ($posts as $post) {
                $post_id = $post['post_id'];
                if (Posts::get_post_images($post_id)) {
                    $post['images'] = Posts::get_post_images($post_id);
                } else {
                    $post['images'][0] = array('image_url' => '/uploads/news-placeholder.png');
                }
                $post['author_name'] = Posts::get_post_author($post['author_id']);
                $post['created_at'] = Posts::date_format(strtotime($post['created_at']), $language);
                $post['updated_at'] = Posts::date_format(strtotime($post['updated_at']), $language);
                $formatted_posts[] = $post;
            }
            $formatted_posts = array_reverse($formatted_posts, true);

            return array('rows' => $formatted_posts);
        }
    }

    //Get posts per page
    public function actionGetPosts()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        //$page_id  = Yii::$app->request->get('page_id');
        //$pagination = new Pagination(['totalCount' => Posts::find()->count()]);
        //$pagination->pageSize = 4;
        //$pagination->page = $page_id;
        $language = Yii::$app->request->get('language', 'ua');

        $posts = Posts::find()
            ->select('title, post_id, author_id, created_at, updated_at, title_ua, excerpt, content')
            ->leftJoin('postLang', '`postLang`.`post_id` = `posts`.`id`')
            ->where(['type' => 'post'])
            ->andWhere(['language' => $language])
            ->andWhere(['not', ['title' => null]])
            ->andWhere(['not', ['content' => null]])
            //->offset($pagination->offset)
            //->limit($pagination->limit)
            ->orderBy('created_at DESC')
            ->asArray()
            ->all();

        $formatted_posts = $this->format_posts($posts, true, $language);

        return array('rows' => $formatted_posts);
    }

    public function actionGetPost()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $post_id = Yii::$app->request->get('post_id');

        return Posts::findOne($post_id);
    }

    public function actionCreate()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $user = new DashboardUsers();
        $request = Yii::$app->request;
        $token = $request->post('token');

        if ($user->checkRole($request)) {
            $user_id = DashboardUsers::getUserId($token);
            $category_id = $request->post('category_id');
            if (!$category_id) {
                $category_id = 1;
            }
            $new_post = new Posts();
            $new_post->author_id = $user_id;
            $new_post->category_id = $category_id;
            $new_post->type = $request->post('type');
            $new_post->updated_at = date('Y-m-d H:i:s');
            $new_post->save();

            $content_params['content'] = $request->post('content');
            $content_params['excerpt'] = $request->post('excerpt');
            $content_params['title'] = $request->post('title');

            $for_empty_params['title_ua'] = $request->post('title');
            Posts::create_post_translation($new_post->id, 'ua', $content_params);
            Posts::create_post_translation($new_post->id, 'ru', $for_empty_params);
            Posts::create_post_translation($new_post->id, 'en', $for_empty_params);

            if ($new_post->id) {
                return array('status' => 'ok', 'message' => 'done', 'post_id' => $new_post->id);
            }
            Yii::$app->response->statusCode = 500;
            $errors = array($new_post->errors);

            return array('status' => 'error', 'message' => array($errors));
        }
        Yii::$app->response->statusCode = 403;

        return array('status' => 'error', 'message' => "you don't have permissions to create posts");
    }

    /**
     * Updates an existing Posts model.
     * If update is successful, the browser will be redirected to the 'view' page.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function actionUpdate()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $user = new DashboardUsers();
        $request = Yii::$app->request;
        $post_id = $request->post('post_id');

        if ($user->checkRole($request) && Posts::findOne(['id' => $post_id])) {
            $updated_post = Posts::findOne($post_id);
            $updated_post->type = $request->post('type');

            $translation = PostLang::find()
                        ->where(['post_id' => $post_id])
                        ->andWhere(['language' => $request->post('language')])
                        ->one();

            $translation->excerpt = $request->post('excerpt');
            $translation->content = $request->post('content');
            $translation->title = $request->post('title');

            if ($request->post('category_id')) {
                $updated_post->category_id = $request->post('category_id');
            }

            if ($updated_post->save() && $translation->save()) {
                return array('status' => 'ok', 'message' => 'done');
            } else {
                Yii::$app->response->statusCode = 500;
                $errors = array($updated_post->errors, $translation->errors);

                return array('status' => 'error', 'message' => array($errors));
            }
        } else {
            Yii::$app->response->statusCode = 403;

            return array('status' => 'error', 'message' => "you don't have permissions to edit posts");
        }
    }

    /**
     * Deletes an existing Posts model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     *
     * @param int $id
     *
     * @return mixed
     */
    public function actionDelete()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $user = new DashboardUsers();
        $request = Yii::$app->request;
        $post_id = $request->post('post_id');
        $post = Posts::findOne(['id' => $post_id]);
        $language = (Yii::$app->request->get('language') ? Yii::$app->request->get('language') : 'ua');
        $post_translation = PostLang::find_translation($post_id, $language);

        if ($user->checkRole($request) && $post && $post_translation) {
            if ($language === 'ua') {
                $post_translation->delete();
                $post->delete();
                Posts::delete_post_images($post_id);

                return array('status' => 'ok', 'message' => 'done');
            } else {
                $post_translation->delete();

                return array('status' => 'ok', 'message' => 'done');
            }
        }
        Yii::$app->response->statusCode = 500;

        return array('status' => 'error',
                    'message' => "permissions error or $post->errors $post_translation->errors", );
    }

    protected function findModel($id)
    {
        if (($model = Posts::findOne($id)) !== null) {
            return $model;
        } else {
            return false;
        }
    }

    // template action for news
    public function actionNews()
    {
        $language = substr(Yii::$app->language, 0, 2);
        $page_id = Yii::$app->request->get('page_id');
        $pagination = new Pagination(['totalCount' => Posts::find()->count()]);
        $pagination->pageSize = 4;
        $pagination->page = $page_id;

        $posts = Posts::find()
            ->select('title, post_id, author_id, created_at, updated_at, title_ua, excerpt, content')
            ->leftJoin('postLang', '`postLang`.`post_id` = `posts`.`id`')
            ->where(['type' => 'post'])
            ->andWhere(['language' => $language])
            ->andWhere(['not', ['title' => null]])
            ->andWhere(['not', ['content' => null]])
            ->offset($pagination->offset)
            ->limit($pagination->limit)
            ->orderBy('created_at ASC')
            ->asArray()
            ->all();
        $posts = $this->format_posts($posts, false, $language)['rows'];
        // dump($posts, 1);

        return $this->render("$language/news", ['posts' => $posts]);
    }
}
