<?php

namespace app\controllers;

use app\models\DashboardRoles;
use app\models\DashboardUsers;
use app\models\DashboardUserToRole;
use Yii;
use yii\base\ErrorException;

class DashboardUsersController extends \yii\web\Controller
{
    public $enableCsrfValidation = false;
    public $file; // add this to your model class

    //Sets response format to JSON
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => \yii\filters\ContentNegotiator::className(),
                'formats' => [
                    'application/json' => \yii\web\Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        //do nothing
    }

    public function actionLogin()
    {
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        $request = Yii::$app->request;

        $roles = new DashboardUsers();

        $result =  $roles->checkUserRole($request);

        if ($result) {
            return $result;
        }

        Yii::$app->response->statusCode = 401;
        return array("status"=>"error","message"=>"You not Admin");

    }

    public function actionRegister()
    {

        $request = Yii::$app->request;

        $user = new DashboardUsers();

        if ($user->checkIfAdmin($request)) {

            try {
                $email_exists = DashboardUsers::find()
                    ->where( [ 'email' => $request->post('email') ] )
                    ->exists();

                $role = DashboardRoles::findOne(['role_name' => $request->post('role')]);

                if ( $role && !$email_exists ) {

                    $user->name = $request->post('name');

                    $user->username = $request->post('username');

                    $user->password = $request->post('password');

                    $user->email = $request->post('email');

                    $user->token = Yii::$app->security->generateRandomString();

                    $user->save();

                    $addRoleToUser = new DashboardUserToRole();

                    $addRoleToUser->role_id = $role->id;

                    $addRoleToUser->user_id = $user->id;

                    $addRoleToUser->save();

                    return array("status"=>"ok", "message"=>"done");
                }
                else {
                    return array("status"=>"error", "message"=>"error");
                }

            }
            catch (ErrorException $e) {
                Yii::warning("SQL Error");
                Yii::$app->response->statusCode = 500;
                return array("status"=>"error", "message"=>"error");
            }

        }
        else
        {
            Yii::$app->response->statusCode = 401;
            return array("status"=>"error","message"=>"You not Admin");

        }

    }

    public function actionAddRole(){

        $request = Yii::$app->request;

        $user = new DashboardUsers();

        if($user->checkIfAdmin($request)) {

            $role = new DashboardRoles();

            if(empty($request->post('name'))) {
                Yii::$app->response->statusCode = 500;
                return array("status"=>"error", "message"=>"role name empty");
            }

            $role_exists = DashboardRoles::find()
                ->where( [ 'role_name' => $request->post('name') ] )
                ->exists();

            if ($role_exists) {
                return array("status"=>"error","message"=>"role already exists");
            }

            $role->role_name  = $request->post('name');

            if (empty($request->post('permissions'))) {
                $role->permissions  = serialize(array('create, edit, delete, update'));
            }
            else {
                $role->permissions  = serialize($request->post('permissions'));
            }


            $role->save();

            return array("status"=>"ok", "message"=>"done");

        } else
            {
                Yii::$app->response->statusCode = 401;
                return array("status"=>"error","message"=>"You not Admin");
            }
    }

    public function actionShowRoles(){
        return DashboardRoles::find()->asArray()->all();
    }

    public function actionEditRole(){

        $request = Yii::$app->request;

        $user = new DashboardUsers();

        if($user->checkIfAdmin($request)) {

            try {
                $role = DashboardRoles::findOne(['id' => $request->post('role_id')]);
                if ($role) {
                    $role->role_name = $request->post('new_role_name');

                    $role->permissions = $request->post('new_role_permissions');

                    $role->save();

                    return array("status"=>"ok", "message"=>"done");
                }
                else {
                    return array("status"=>"error","message"=>"role not found");
                }

            } catch (ErrorException $e) {

                Yii::warning("SQL Error");
                Yii::$app->response->statusCode = 500;
                return array("status"=>"error","message"=>"SQL server error");
            }

        } else
        {
            Yii::$app->response->statusCode = 401;
            return array("status"=>"error","message"=>"You are not Admin");
        }


    }

    public function actionDeleteRole(){

        $request = Yii::$app->request;

        $user = new DashboardUsers();

        if($user->checkIfAdmin($request)) {

            try {

                $role = DashboardRoles::findOne(['id' => $request->post('role_id')]);
                if ($role) {
                    $role->delete();
                    return array("status"=>"ok", "message"=>"done");
                }
                else {
                    return array("status"=>"error","message"=>"role not found");
                }

            }catch (ErrorException $e) {
                Yii::warning("SQL Error");
                Yii::$app->response->statusCode = 500;
                return array("status"=>"error","message"=>"SQL server error");
            }

        } else
        {
            Yii::$app->response->statusCode = 401;
            return array("status"=>"error","message"=>"You not Admin");

        }

    }


    public function actionShowUsers(){

        $request = Yii::$app->request;

        $user = new DashboardUsers();

        if($user->checkIfAdmin($request)) {

            return DashboardUsers::find()->select('dashboard_users.id,dashboard_users.name,dashboard_users.username,dashboard_users.email, dashboard_roles.role_name')
                ->join('LEFT JOIN','dashboard_user_to_role', 'dashboard_user_to_role.user_id = dashboard_users.id')
                ->join('LEFT JOIN','dashboard_roles', 'dashboard_roles.id = dashboard_user_to_role.role_id')
                ->asArray()
                ->all();


        } else
        {
            Yii::$app->response->statusCode = 401;
            return array("status"=>"error","message"=>"You not Admin");

        }

    }

    public function actionEditUser(){

        $request = Yii::$app->request;

        $user = new DashboardUsers();

        if($user->checkIfAdmin($request)) {

            try {

                $client = DashboardUsers::findOne($request->post('user_id'));

                if ($client) {
                    $client->name = $request->post('new_user_name');

                    $client->username = $request->post('new_user_username');

                    $client->email = $request->post('new_user_email');

                    $client->save();

                    $role = DashboardRoles::findOne(['role_name' => $request->post('new_user_role')]);

                    $editRoleToUser = DashboardUserToRole::findOne(['user_id' => $client->id]);

                    $editRoleToUser->role_id = $role->id;

                    $editRoleToUser->user_id = $client->id;

                    $editRoleToUser->save();

                    return array("status"=>"ok", "message"=>"done");
                }
                else {
                    return array("status"=>"error","message"=>"user not found");
                }


            } catch (ErrorException $e) {

                Yii::warning("SQL Error");
                Yii::$app->response->statusCode = 500;
                return array("status"=>"error","message"=>"SQL server error");
            }

        }else
        {
            Yii::$app->response->statusCode = 401;
            return array("status"=>"error","message"=>"You not Admin");

        }

    }

    public function actionDeleteUser(){

        $request = Yii::$app->request;

        $user = new DashboardUsers();

        if($user->checkIfAdmin($request)) {

            try {

                $client = DashboardUsers::findOne($request->post('user_id'));
                $deleteRoleToUser = DashboardUserToRole::findOne(['user_id' => $client->id]);

                if ($client && $deleteRoleToUser ) {
                    $deleteRoleToUser->delete();
                    $client->delete();
                    return array("status"=>"ok", "message"=>"done");
                }
                else {
                    return array("status"=>"error","message"=>"user or role not found");
                }

            }catch (ErrorException $e) {

                Yii::warning("SQL Error");

                return array("status"=>"ok", "message"=>"done");
            }
        }
        else
        {
            Yii::$app->response->statusCode = 401;
            return array("status"=>"error","message"=>"You not Admin");

        }

    }

    public function SaveAvatar($avatar) {

    }

    /*
    public function ResetToken() {
        $request = Yii::$app->request;
        $user_email = $request->post('email');
        $user = DashboardUsers::findOne(['email' => $user_email]);
        $user->token = $this->generate_new_token();
        $user->save();
        $this->send_new_token($user_email, $user->token);
    }


    private function generate_new_token() {
        return $this->generatePasswd();
    }

    private function send_new_token($user_email, $token) {
        $message = Yii::$app->mailer->compose();
        $message->setFrom('admin@zlata-trade.com')
        $message->setTo($user_email)
            ->setSubject('Token')
            ->setTextBody("Your new token $token")
            ->send();
    }

    private function generatePasswd($numAlpha=6,$numNonAlpha=2)
    {
        $listAlpha = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $listNonAlpha = ',;:!?.$/*-+&@_+;./*&?$-!,';
        return str_shuffle(
            substr(str_shuffle($listAlpha),0,$numAlpha) .
            substr(str_shuffle($listNonAlpha),0,$numNonAlpha)
        );
    }
    */
}
